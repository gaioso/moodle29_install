# Instalación de Moodle 29

* [Máquina virtual](maquina_virtual.md)
* [Permisos sudo](permisos_sudo.md)
* [Instalacións previas](instalacions_previas.md)
* [Descarga de ficheiros](descarga_de_ficheiros.md)
* [Protexer os ficheiros de Moodle](protexer_os_ficheiros_de_moodle.md)
* [Crear unha nova base de datos](crear_unha_nova_base_de_datos.md)
* [Crear o directorio de datos](crear_o_directorio_de_datos.md)
  * [Engadir novo disco](engadir_novo_disco.md)
  * [Permisos no directorio](permisos_no_directorio.md)
* [Instalación de Moodle](instalacion_de_moodle.md)
* [Tamaño máximo dos ficheiros subidos](tamaño_maximo_dos_ficheiros_subidos.md)
* [Adaptación de tema](adaptacion_de_tema.md)
  * [Eliminar bloques da portada](eliminar_bloques_da_portada.md)
  * [Cambios no estilo CSS](cambios_no_estilo_css.md)
* [Configuración do correo](configuracion_de_correo.md)
* [Configuración de cron](configuracion_de_cron.md)
* [Restauración de cursos](restauracion_de_cursos.md)
  * [Resultados das probas](resultados_das_probas.md)
* [Aspectos da seguridade](aspectos_da_seguridade.md)
