# Máquina virtual

## Hardware

* Procesadores
  * 2 (1 socket, 2 cores) [qemu32]
* Memoria
  * 1.00 GB
* Disco duro (ide0)
  * vg:vm-103-disk-1,size=16G [raw]

## Rede

<pre>
allow-hotplug eth0
iface eth0 inet static
	address 193.144.61.70
	netmask 255.255.255.192
	network 193.144.61.64
	broadcast 193.144.61.127
	gateway 193.144.61.65
	# dns-* options are implemented by the resolvconf package, if installed
	dns-nameservers 193.144.48.30 193.144.48.100
	dns-search udc.es
</pre>

## Software

* Sistema operativo
<pre>
Distributor ID: Debian
Description:    Debian GNU/Linux 8.1 (jessie)
Release:        8.1
Codename:       jessie
</pre>
* Servizos iniciais
  * Web server
    * Apache/2.4.10 (Debian)
  * SSH server
    * OpenSSH_6.7p1 Debian-5, OpenSSL 1.0.1k 8 Jan 2015

* Usuarios
  * root / V*****p
  * osl / reverse
