# Resultados das probas

## Contas de usuario
Despois da restauración dun curso cos datos dos usuarios matriculados engadidos, observamos que se realizar a migración completa da conta de usuario, incluídas as credenciais.

Se isto se confirma non é necesario realizar unha exportación manual das contas de usuario, xa que segundo a documentación, non hai un xeito doado de migrar o contrasinal.

Por suposto, aquelas contas de usuario que non estean matriculadas en ningún curso dos restaurados non aparecerá na nova versión de Moodle. Pode ser un xeito de realizar unha limpeza de usuarios non activos na plataforma.
