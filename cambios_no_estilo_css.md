# Cambios no estilo CSS

O tema por defecto incorpora o seguinte texto na cabeceira:
> Development version - test server only

Para poder eliminalo temos que modificar o ficheiro *.../theme/essential/style/essential.css*
<pre>
/* Eliminar texto do header - 7/8/15 - osl@cixug.es
div#page::before { content: "Development version - test server only."; font-size: 4em; margin-top: 24px; margin-bottom: 24px; line-height: 42px; text-align: center; }
*/

div#page::before { font-size: 4em; margin-top: 24px; line-height: 42px; text-align: center; }
</pre>
