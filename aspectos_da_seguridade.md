# Aspectos da seguridade

## Permisos no directorio principal de Moodle.

Seguindo as recomendacións da documentación de Moodle: https://docs.moodle.org/29/en/Security_recommendations
aplicamos os seguintes cambios:
<pre>
chown -R root:root /var/www/moodle/
find /var/www/moodle/ -type d -exec chmod 755 {} \;
find /var/www/moodle/ -type f -exec chmod 644 {} \;
</pre>

> Antes de aplicar os permisos aos directorios, non era posible acceder a certos recursos dos cursos. Parece que é necesario acceder a algún ficheiro PHP para poder visualizar certos contidos.

## Permisos no directorio de datos _moodledata_.

<pre>
# chown -R www-data: /var/moodledata/
# find /var/moodledata/ -type d -exec chmod 700 {} \;
# find /var/moodledata/ -type f -exec chmod 600 {} \;
</pre>
