# Crear unha nova base de datos

> https://docs.moodle.org/29/en/MySQL

A pesar do indicado na documentación respecto do CHARACTER SET, imos deixar o valor por defecto para esta propiedade (utf8). Executamos polo tanto o seguinte comando para crear unha nova base de datos baleira:
<pre>
# mysqladmin create moodle -u root -p
Enter password:
</pre>

Para asignar os permisos de usuario na base de datos, imos evitar o uso da ferramenta _mysql_setpermission_, xa que non incorpora unha opción automática que inclúa o permiso ALTER. Entendo que este permiso vai ser necesario á hora de poder realizar actualizacións na versión de Moodle. Polo tanto imos executar o comando de creación de usuario e permisos desde a consola de MySQL, tal e como se indica na documentación:
<pre>
mysql> GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO moodleuser@localhost IDENTIFIED BY '2015Moodle.';
</pre>

...e seguindo o consello da documentación:
> Make sure you invent a strong password and resist the temptation to 'GRANT ALL'.
