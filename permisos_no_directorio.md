# Permisos no directorio

Unha vez creado e montado o directorio de datos 'moodledata', vanse establecer os novos permisos:
<pre>
# chown -R root:www-data /var/moodledata
# chmod -R g=rwx,o= /var/moodledata
</pre>

Co cal, deixamos como propietario a _root_ e grupo propietario a _www-data_, asignándolle permisos completos ao mesmo. O resto de usuarios do sistema non teñen ningún permiso.
