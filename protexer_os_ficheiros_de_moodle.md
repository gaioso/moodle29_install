# Protexer os ficheiros de Moodle

Tal e como indica a documentación, é necesario protexer o acceso aos ficheiros que se decargaron no paso anterior. O usuario do servidor web non poderá escribir neles.
> Estes comandos non os executaremos polo momento.

<pre>
# chown -R root /var/www/moodle
# chmod -R 0755 /var/www/moodle
# find /var/www/moodle -type f -exec chmod 0644 {} \;
</pre>

Pero, se queremos facer uso da ferramenta de instalación que incopora Moodle, teremos que permitir a escritura no directorio de instalación (por exemplo, para crear o ficheiro _config.php_). Polo que de momento, o que teremos que facer é cambiar o propietario dese directorio de xeito recursivo.
<pre>
# chown -R www-data /var/www/moodle
</pre>
