# Permisos sudo

Na configuración por defecto do servizo SSH non está permitido o acceso do usuario root empregando o contrasinal, tal e como se pode comprobar no ficheiro de configuración:

* */etc/ssh/sshd_config*
<pre>
...
# Authentication:
LoginGraceTime 120
PermitRootLogin without-password
StrictModes yes
...
</pre>

e como se pode comprobar no manual de sshd_config:
<pre>
$ man sshd_config
...
PermitRootLogin
             Specifies whether root can log in using ssh(1).  The argument must be “yes”, “without-password”, “forced-commands-only”, 
             or “no”.  The default is “yes”.

             If this option is set to “without-password”, password authentication is disabled for root.
...
</pre>

polo que é necesario executar os seguintes comandos desde a consola de Proxmox:
<pre>
# aptitude install sudo
...
# usermod -aG sudo osl
</pre>
a partir dese momento, o usuario _osl_ pode conectarse vía SSH e executar comandos con privilexios de _root_.
