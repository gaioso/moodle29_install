# Instalación de Moodle

Executaremos o script de instalación a través da liña de comandos co usuario de apache (www-data):

<pre>
# cd /var/www/moodle/admin/cli
# sudo -u www-data /usr/bin/php install.php --chmod=2770 --lang=gl --wwwroot=http://193.144.61.70 --dataroot=/var/moodledata \
--dbuser=moodleuser --dbpass=2015Moodle. --fullname="Aula virtual CIXUG" --shortname=cixug_moodle --adminuser=administrator --adminpass=2015Moodle. --agree-license
</pre>

Na primeira execución amosou o seguinte erro:
<pre>
-->block_course_summary
++ Correcto ++
Default exception handler: Mágoa, o engadido «block_feedback» está defectuoso ou desactualizado, non é posíbel continuar. \
  Debug: Conflito de denominación: o bloque feedback ten o mesmo título que un bloque xa existente comments!
Error code: detectedbrokenplugin
* line 862 of /lib/upgradelib.php: plugin_defective_exception thrown
* line 426 of /lib/upgradelib.php: call to upgrade_plugins_blocks()
* line 1630 of /lib/upgradelib.php: call to upgrade_plugins()
* line 486 of /lib/installlib.php: call to upgrade_noncore()
* line 773 of /admin/cli/install.php: call to install_cli_database()

!!! Mágoa, o engadido «block_feedback» está defectuoso ou desactualizado, non é posíbel continuar. !!!
!! Conflito de denominación: o bloque feedback ten o mesmo título que un bloque xa existente comments!
Error code: detectedbrokenplugin !!
!! Stack trace: * line 862 of /lib/upgradelib.php: plugin_defective_exception thrown
* line 426 of /lib/upgradelib.php: call to upgrade_plugins_blocks()
* line 1630 of /lib/upgradelib.php: call to upgrade_plugins()
* line 486 of /lib/installlib.php: call to upgrade_noncore()
* line 773 of /admin/cli/install.php: call to install_cli_database()
 !!
</pre>

Chegados a este punto, se volvemos a executar o script _install.php_ amosa o seguinte erro:
<pre>
Xa existe o ficheiro config.php. Empregue admin/cli/upgrade.php se quere actualizar o seu sitio web.
</pre>

Unha vez eliminado ese ficheiro, podemos volver a lanzar o script, pero cando chega o momento de actualizar a base de datos, amosa o seguinte erro:
<pre>
Xa existen táboas de base de datos, a instalación CLI non pode continuar.
</pre>

E neste punto, intentamos realizar unha actualización da base de datos, mediante o script _upgrade.php_:
<pre>
# sudo -u www-data /usr/bin/php upgrade.php
</pre>

proceso que completa a creación das táboas a partir de _block_feedback_.
<pre>
[...]
-->logstore_standard
++ Correcto ++
A anovación por liña de ordes completouse satisfactoriamente.
</pre>
