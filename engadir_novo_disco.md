# Engadir novo disco

Despois de parar a máquina, engadimos un novo disco de 10GB en formato RAW co seguinte descrición: *vg:vm-103-disk-2*.

Creamos unha nova partición de tipo 8e LVM con fdisk.
<pre>
Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): 

Using default response p.
Partition number (1-4, default 1): 
First sector (2048-20971519, default 2048): 
Last sector, +sectors or +size{K,M,G,T,P} (2048-20971519, default 20971519): 

Created a new partition 1 of type 'Linux' and of size 10 GiB.
</pre>

A continuación, imos crear un novo Physical Volume, e engadilo ao Volume Group existente:
<pre>
# pvcreate /dev/sdb1
  Physical volume "/dev/sdb1" successfully created
# vgextend cixug-moodle-vg /dev/sdb1
  Volume group "cixug-moodle-vg" successfully extended
# vgs
  VG              #PV #LV #SN Attr   VSize  VFree 
  cixug-moodle-vg   2   5   0 wz--n- 25,75g 15,10g
</pre>

Creamos un novo Logical Volume:
<pre>
# lvcreate -n moodledata -L 10G cixug-moodle-vg
  Logical volume "moodledata" created
</pre>

Dámoslle formato con tipo ext4:
<pre>
# mkfs.ext4 /dev/mapper/cixug--moodle--vg-moodledata>
</pre>

Creamos o punto de montaxe, e modificamos o fstab:
<pre>
# mkdir /var/moodledata

[...]
/dev/mapper/cixug--moodle--vg-moodledata /var/moodledata ext4  defaults   0  2
[...]
</pre>

A configuración final do sistema de ficheiros queda do seguinte xeito:
<pre>
# df -h
Sist. Fich                               Tamaño Usado  Disp Uso% Montado en
/dev/dm-0                                  5,4G  838M  4,2G  17% /
udev                                        10M     0   10M   0% /dev
tmpfs                                      202M  4,4M  198M   3% /run
tmpfs                                      504M     0  504M   0% /dev/shm
tmpfs                                      5,0M     0  5,0M   0% /run/lock
tmpfs                                      504M     0  504M   0% /sys/fs/cgroup
/dev/mapper/cixug--moodle--vg-tmp          360M  2,1M  335M   1% /tmp
/dev/mapper/cixug--moodle--vg-var          2,7G  450M  2,1G  18% /var
/dev/sda1                                  236M   32M  192M  15% /boot
/dev/mapper/cixug--moodle--vg-home         880M  7,7M  806M   1% /home
/dev/mapper/cixug--moodle--vg-moodledata   9,8G   23M  9,2G   1% /var/moodledata
</pre>
