# Tamaño máximo dos ficheiros subidos

Seguimos as indicacións da documentación de Moodle:
https://docs.moodle.org/29/en/File_upload_size

É importante coñecer a prioridade que establece Moodle á hora de subir ficheiros:
<pre>
Server level
Moodle site level
Course level
Activity level
</pre>

Polo tanto, aplicamos os seguintes cambios no ficheiro php.ini:
<pre>
# vi /etc/php5/apache2/php.ini
[...]
post_max_size = 500M
[...]
upload_max_filesize = 500M
[...]
max_execution_time = 600

# service apache2 reload
</pre>

Comprobamos na seguinte ruta a configuración de Moodle:
> Administración - Seguranza - Políticas do sitio - Tamaño máximo do ficheiro enviado
