# Instalacións previas

Antes de executar a instalación de Moodle é necesario realizar as seguintes instalacións de módulos PHP, Git e o servidor de MySQL.
<pre>
# aptitude install php5 php5-mysql php5-gd php5-curl git mysql-server php5-xmlrpc php5-intl
</pre>

* Credenciais MySQL: root / toor
