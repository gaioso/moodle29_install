# Restauración de cursos

Realizanse probas de restauración das copias de seguridade dos cursos activos no moodle 2.6 actual.

## Administración de sistemas GNU/Linux - LPIC1

* O ficheiro coa copia de seguranza do curso ocupa un total de 22 MB, e a subida e restauración realizouse correctamente.

## Curso de posgrado de especialización en orientación laboral Ed. 2015

* O ficheiro coa copia de seguranza ocupan un total de 382 MB. A subida non funcionou correctamente, amosando o seguinte erro:
<pre>Failed to write file to disk</pre>

Despois de realizar varias comprobacións no ficheiro php.ini de PHP, observamos que o problema está no espacio do LV montado no directorio /tmp.
Este LV ten un espazo total de 360MB, co cal, á hora de subir un ficheiro, apache fai uso dese directorio temporalmente. A solución pasa por aumentar o espazo dese LV.

# Paramos os servizos que poden facer uso do directorio /tmp
<pre>
# service apache2 stop
# service mysql stop
</pre>
# Desmontamos, e aumentamos o tamaño do LV
<pre>
# umount /tmp
# lvextend --size +1G /dev/cixug-moodle-vg/tmp
# e2fsck -f /dev/cixug-moodle-vg/tmp
# resize2fs /dev/cixug-moodle-vg/tmp
# mount /tmp
</pre>
# Iniciamos de novo os servizos parados
<pre>
# service mysql start
# service mysql stop
</pre>

Despois deste cambio, a restauración funcionou correctamente.

## Primeiros pasos con LibreOffice

* O ficheiro coa copia de seguranza ocupa un total de 1,0GB
