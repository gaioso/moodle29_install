# Eliminar bloques da portada

Na configuración por defecto da páxina de portada (antes de iniciar sesión) aparece o bloque Calendario, e desexamos agochalo para que non se poida consultar polos convidados.

Iniciamos sesión como _administrador_, e no bloque Navegación facemos clic no apartado *Inicio do sitio*, de tal xeito que na URL aparece engadido o parámetro *?redirect=0*. Unha vez situados nesa páxina, imos ao bloque Administración para *Activar a edición*.

Se queremos agochar o Calendario, faremos clic no engranaxe de configuración para seleccionar *Agochar o bloque Calendario*.

Do mesmo xeito tamén podemos engadir novos bloques.
