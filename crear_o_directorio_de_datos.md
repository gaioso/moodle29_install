# Crear o directorio de datos

Este directorio vai servir para almacenar os ficheiros subidos, a caché, ficheiros temporais, etc., e non pode ser accesible desde o exterior a través do servidor web. Polo tanto imos a crealo directamente en /var, tendo en conta que o seu tamaño pode chegar a medrar de xeito considerable. Polo tanto, imos reducir o espazo do LV dedicado a /home e crear un novo LV para este directorio.

<pre>
# umount /home
# e2fsck -f /dev/mapper/cixug--moodle--vg-home
# resize2fs -p /dev/mapper/cixug--moodle--vg-home 1G
# lvreduce -L 1G /dev/mapper/cixug--moodle--vg-home
# mount /home
</pre>

> Chegados a este punto, observamos que o directorio actual en produción ten un tamaño ocupado de 25GB, nun disco de 50GB. Polo que teremos que modificar a estratexia respecto a este directorio.

> A opción a estudiar vai ser a incorporación dun novo disco a LVM, para empregalo únicamente como almacenaxe deste directorio. Polo momento, crearemos un novo disco en Proxmox de 10GB de tamaño. Entendo que cando se realice a migración cara os servidores da UDC, este disco poderá ser redimensionado polo SIC.
