# Descarga de ficheiros

Seguindo a recomendación que aparece na documentación de Moodle, imos realizar un clonado do proxecto a través de Git. Deste xeito, tamén se facilitan as posteriores actualizacións.
<pre>
# cd /var/www
# git clone --depth=1 -b MOODLE_29_STABLE git://git.moodle.org/moodle.git
</pre>
